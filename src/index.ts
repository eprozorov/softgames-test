import { Application } from "pixi.js";
import { Game } from "./softgames/game";
const TWEEN = require("@tweenjs/tween.js");

export const WIDTH = 1280;
export const HEIGHT = 720;
export const currentSizes = { width: WIDTH, height: HEIGHT };
const resolution = Math.floor(Math.max(1, Math.min(3, window.devicePixelRatio)));
const app = new Application({
  width: WIDTH,
  height: HEIGHT,
  autoDensity: true,
  resolution,
  backgroundColor: 0x2f9443,
  sharedTicker: true,
});
document.body.appendChild(app.view);
const game = new Game(app.stage);

app.ticker.add(() => {
  TWEEN.update(app.ticker.lastTime);
});
const calculateCanvasSize = () => {
  const scale: number =
    innerWidth > innerHeight
      ? Math.max(HEIGHT / innerHeight, WIDTH / innerWidth)
      : Math.max(WIDTH / innerHeight, HEIGHT / innerWidth);
  return {
    width: Math.ceil(innerWidth * scale),
    height: Math.ceil(innerHeight * scale),
  };
};
const resize = () => {
  const { width, height } = calculateCanvasSize();
  currentSizes.width = width;
  currentSizes.height = height;
  app.renderer.resize(width, height);
  game.resize(width, height);
  const { style } = app.view;
  style.width = `${innerWidth}px`;
  style.height = `${innerHeight}px`;
};
window.addEventListener("resize", resize);
resize();
