import { Container, Loader } from "pixi.js";
import { Menu } from "./menu";
import { Preloader } from "./preloader";
import { AbstractTask } from "./tasks/abstract-task";
import { Task0 } from "./tasks/task0";
import { Task1 } from "./tasks/task1";
import { Task2 } from "./tasks/task2";

export class Game {
  private readonly mainContainer: Container;
  private menu: Menu;
  private task: AbstractTask;
  constructor(stage: Container) {
    this.mainContainer = new Container();
    stage.addChild(this.mainContainer);

    const preloader = new Preloader();
    preloader.on("onLoaded", () => {
      this.mainContainer.removeChild(preloader);
      preloader.destroy({ children: true });
      this.start();
    });
    this.mainContainer.addChild(preloader);
  }
  private start() {
    this.menu = new Menu();
    this.menu.on("start", this.onStartTask.bind(this));
    this.mainContainer.addChild(this.menu);
  }
  private onStartTask(taskId: number) {
    this.menu.off("start");
    this.mainContainer.removeChild(this.menu);
    this.menu.destroy();

    this.startTask(taskId);
  }
  private startTask(taskId: number) {
    switch (taskId) {
      case 0:
        this.task = new Task0();
        break;
      case 1:
        this.task = new Task1();
        break;
      case 2:
        this.task = new Task2(Loader.shared.resources.fire.data);
        break;
    }
    this.task.on("close", () => {
      this.mainContainer.removeChild(this.task);
      this.task.destroy();
      this.start();
    });
    this.task.start();
    this.mainContainer.addChild(this.task);
  }
  public resize(width: number, height: number) {
    this.mainContainer.position.set(width * 0.5, height * 0.5);
    this.task && this.task.resize();
  }
}
