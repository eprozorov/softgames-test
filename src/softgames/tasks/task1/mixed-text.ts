import {
  ITextStyle,
  Text,
  TextStyle,
  TextMetrics,
  Container,
  Texture,
  Sprite,
  IPointData,
} from "pixi.js";
import { SpritesPool } from "../../utils/sprites-pool";
import { TextsPool } from "../../utils/texts-pool";

export class MixedText extends Container {
  private static txtPool: TextsPool = new TextsPool().init(10);
  private static imgPool: SpritesPool = new SpritesPool().init(10);
  private static imgReg: RegExp = /\^i\{.+?\}i\^/g;
  private _maxWidth: number = -1;
  private txt: string;
  private _style: Partial<ITextStyle> | TextStyle;
  private dirty: boolean;
  private parsedTexts: string[];
  private parsedImages: string[];
  private txtItems: Text[] = [];
  private imgItems: Sprite[] = [];
  constructor(text: string, style?: Partial<ITextStyle> | TextStyle) {
    super();
    this.dirty = true;
    this._style = style;
    this.text = text;
  }
  public set style(s: Partial<ITextStyle> | TextStyle) {
    if (s === this._style) return;
    this._style = s;
    this.dirty = true;
    this.updateText();
  }
  public get maxWidth() {
    return this._maxWidth;
  }
  public set maxWidth(w: number) {
    if (this._maxWidth === w) return;
    this.dirty = true;
    this._maxWidth = w;
    this.updateText();
  }
  public set text(t: string) {
    if (this.txt === t) return;
    this.dirty = true;
    this.txt = t;
    this.parsedTexts = this.txt.split(MixedText.imgReg);
    this.parsedImages = this.txt.match(MixedText.imgReg)?.map((value) => {
      return value.substring(3, value.length - 3);
    });
    this.updateText();
  }
  public destroy() {
    this.clean();
    super.destroy();
  }
  protected updateText() {
    if (!this.dirty) return;
    this.clean();
    let pos = { x: 0, y: 0 };
    let maxHeightInLine = 0;
    for (let i = 0; i < this.parsedTexts.length; i++) {
      const t = this.parsedTexts[i];
      const txt = MixedText.txtPool.obtain();
      txt.style = this._style;
      txt.text = t;
      this.txtItems.push(txt);
      maxHeightInLine = Math.max(txt.height, maxHeightInLine);
      txt.position.copyFrom(pos);
      this.addChild(txt);
      pos = this.getNewPos(pos, txt.width, maxHeightInLine);

      if (this.parsedImages && this.parsedImages[i]) {
        const sprite = MixedText.imgPool.obtain();
        sprite.texture = Texture.from(this.parsedImages[i]);
        this.imgItems.push(sprite);
        maxHeightInLine = Math.max(sprite.height, maxHeightInLine);
        sprite.anchor.y = 0.5;
        sprite.position.copyFrom(pos);
        sprite.position.y += txt.height * 0.5;
        this.addChild(sprite);
        pos = this.getNewPos(pos, sprite.width, maxHeightInLine);
      }
    }
    this.dirty = false;
  }
  private clean() {
    for (let i = this.txtItems.length - 1; i >= 0; i--) {
      MixedText.txtPool.release(this.txtItems[i]);
      this.txtItems.splice(i, 1);
    }
    for (let i = this.imgItems.length - 1; i >= 0; i--) {
      MixedText.imgPool.release(this.imgItems[i]);
      this.imgItems.splice(i, 1);
    }
    this.removeChildren();
  }
  private getNewPos(
    pos: IPointData,
    width: number,
    height: number
  ): IPointData {
    if (pos.x + width > this._maxWidth) {
      pos.x = 0;
      pos.y += height + 10;
    } else {
      pos.x += width;
    }

    return pos;
  }
}
