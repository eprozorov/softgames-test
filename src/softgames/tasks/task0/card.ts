import { Tween } from "@tweenjs/tween.js";
import { Sprite, Texture } from "pixi.js";

export class Card extends Sprite {
  constructor(id: number) {
    super();
    this.texture = Texture.from(`card${id}.png`);
  }
  public moveTo({ x, y }, ms: number) {
    const tween = new Tween(this.position);
    tween.to({ x, y }, ms).start();
  }
}
