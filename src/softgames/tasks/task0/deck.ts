import { Container } from "pixi.js";
import { HEIGHT } from "../../..";
import { Card } from "./card";

export class Deck extends Container {
  private readonly cardHeight = 240;
  private size: number;
  private cards: Card[] = [];
  private topY: number;
  private stepY: number;
  constructor() {
    super();
  }
  public init(size: number) {
    this.size = size;
    this.topY = -HEIGHT * 0.5;
    this.stepY = Math.min(30, (HEIGHT - this.cardHeight) / this.size);
  }
  public generateDeck() {
    if (!this.size)
      throw new Error("Deck was not inited. please run .init(size) first");
    let y = this.topY;
    for (let i = 0; i < this.size; i++) {
      const card = new Card(this.getRandomCard());
      this.addChild(card);
      this.cards.push(card);
      card.position.y = y;
      y += this.stepY;
    }
  }
  public async animateCardAndAdd(card: Card) {
    const global = card.toGlobal({ x: 0, y: 0 });
    this.addChild(card);
    card.position = this.toLocal(global);
    card.moveTo(this.activeCardPoint, 2000);
    this.cards.push(card);
    //this.resize();
  }
  public getActiveCardAndRemove() {
    return this.cards.pop();
  }
  /*public resize() {
        let y = - currentSizes.height * 0.5;
        const step = Math.min(30, (currentSizes.height - this.cardHeight) / this.cards.length);
        this.cards.forEach((card)=> {
            card.position.y = y;
            y += step;
        });
    }*/
  private get activeCardPoint() {
    return { x: 0, y: this.topY + this.cards.length * this.stepY };
  }
  private getRandomCard() {
    return Math.floor(Math.random() * 52);
  }
}
