import { clearTickerInterval, setTickerInterval } from "../utils/intervals";
import { AbstractTask } from "./abstract-task";
import { MixedText } from "./task1/mixed-text";

export class Task1 extends AbstractTask {
  private static texts = [
    "Hello, ",
    " amazing ",
    " Bob ",
    " LoL ",
    "Welcome . ",
    " No! ",
    " Yes! ",
  ];
  private static emojes = ["emoj0.png", "emoj1.png", "emoj2.png", "emoj3.png"];
  private interval: any;
  private txt: MixedText;
  constructor() {
    super();
  }
  public start() {
    this.txt = new MixedText("");
    this.txt.maxWidth = 500;
    this.addChild(this.txt);

    this.resize();

    this.interval = setTickerInterval(() => {
      this.changeText();
    }, 1000);
    this.changeText();
  }
  public resize() {
    super.resize();
    this.txt.position.set(-this.txt.width * 0.5, -this.txt.height * 0.5);
  }
  public destroy() {
    this.txt.destroy();
    clearTickerInterval(this.interval);
    super.destroy();
  }
  protected changeText() {
    this.txt.style = { fontSize: Math.floor(Math.random() * 30 + 10) };
    this.txt.text = this.getRandomMixedText();
    this.resize();
  }
  private getRandomMixedText(): string {
    let t = "";
    const tAmount = Math.floor(Math.random() * 10 + 1);
    for (let i = 0; i < tAmount; i++) {
      const index = Math.floor(Math.random() * Task1.texts.length);
      t += Task1.texts[index];
      if (Math.random() < 0.4)
        t += ` ^i{${
          Task1.emojes[Math.floor(Math.random() * Task1.emojes.length)]
        }}i^ `;
    }
    return t;
  }
}
