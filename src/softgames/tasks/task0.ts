import { Container, Text, Ticker } from "pixi.js";
import { currentSizes, HEIGHT } from "../..";
import { clearTickerInterval, setTickerInterval } from "../utils/intervals";
import { AbstractTask } from "./abstract-task";
import { Deck } from "./task0/deck";

export class Task0 extends AbstractTask {
  private deck0: Deck;
  private deck1: Deck;
  private fps: Text;
  private interval: any;
  private decks: Container;
  constructor() {
    super();
  }
  public start() {
    this.decks = new Container();
    this.addChild(this.decks);
    const decksSize = 144;
    this.deck0 = new Deck();
    this.deck0.init(decksSize);
    this.deck0.generateDeck();
    this.deck1 = new Deck();
    this.deck1.init(decksSize);
    this.decks.addChild(this.deck0, this.deck1);
    this.deck0.x = -200;
    this.deck1.x = 30;
    this.createFPSMeter();
    this.resize();
    this.startMovingCards();
  }
  public resize() {
    super.resize();
    this.decks.scale.set(Math.min(1, HEIGHT / currentSizes.height));
    this.fps.position.set(
      -currentSizes.width * 0.5 + 10,
      -currentSizes.height * 0.5 + 10
    );
  }
  public destroy() {
    Ticker.shared.remove(this.updateFPS, this);
    this.stopMovingCards();
    super.destroy();
  }
  private createFPSMeter() {
    this.fps = new Text("0", { fill: 0x000000, fontSize: "40px" });
    this.addChild(this.fps);
    Ticker.shared.add(this.updateFPS, this);
  }
  private updateFPS() {
    this.fps.text = `FPS: ${Ticker.shared.FPS.toFixed(0)}`;
  }
  private stopMovingCards() {
    clearTickerInterval(this.interval);
  }
  private startMovingCards() {
    this.moveCard();
    this.interval = setTickerInterval(() => {
      this.moveCard();
    }, 1000);
  }
  private moveCard() {
    const card = this.deck0.getActiveCardAndRemove();
    if (card) {
      this.deck1.animateCardAndAdd(card);
    } else {
      this.stopMovingCards();
    }
  }
}
