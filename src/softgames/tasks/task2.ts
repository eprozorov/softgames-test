import { Emitter } from "pixi-particles";
import { BLEND_MODES, Container, Ticker } from "pixi.js";
import { AbstractTask } from "./abstract-task";

export class Task2 extends AbstractTask {
  private emitter: Emitter;
  private emitterData: any;
  constructor(emitterData: any) {
    super();
    this.emitterData = emitterData;
  }
  public start() {
    const fire = new Container();
    this.addChild(fire);
    this.emitter = new Emitter(fire, ["e1.png", "e2.png"], this.emitterData);
    this.emitter.particleBlendMode = BLEND_MODES.ADD;
    this.emitter.emit = true;
    Ticker.shared.add(this.update, this);
    this.resize();
  }
  public destroy() {
    this.emitter.emit = false;
    this.emitter.destroy();
    super.destroy();
  }
  private update(t: number) {
    this.emitter.update(t * 0.0005);
  }
}
