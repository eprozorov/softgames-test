import { Container } from "pixi.js";
import { currentSizes } from "../..";
import { Button } from "../button";

export abstract class AbstractTask extends Container {
  protected closeButton: Button;
  constructor() {
    super();
    this.addCloseButton();
  }
  public abstract start();
  public resize() {
    const { width, height } = this.closeButton;
    this.closeButton.position.set(
      currentSizes.width * 0.5 - width,
      -currentSizes.height * 0.5 + height
    );
  }
  protected addCloseButton() {
    this.closeButton = new Button();
    this.closeButton.text = "X";
    this.addChild(this.closeButton);
    this.closeButton.on("pointerdown", () => {
      this.emit("close");
    });
  }
}
