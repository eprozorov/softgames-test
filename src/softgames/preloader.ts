import { Container, Loader, Text } from "pixi.js";

export class Preloader extends Container {
  private txt: Text;
  constructor() {
    super();
    this.txt = new Text("", { fill: 0, fontSize: "30px" });
    this.txt.anchor.set(0.5, 0.5);
    this.addChild(this.txt);
    this.progress = 0;

    this.startLoading();
  }

  public set progress(value: number) {
    this.txt.text = `Loading: ${value}%`;
  }
  private startLoading() {
    const loader = Loader.shared;
    const loadOptions = { crossOrigin: true };
    loader.baseUrl = "assets/";
    loader.add("cards", "spritesheets/cards.json", loadOptions);
    loader.add("fire", "fire.json", loadOptions);
    loader.onProgress.add(
      (loader: { progress: number }) => (this.progress = loader.progress)
    );
    loader.load(() => {
      this.emit("onLoaded");
    });
  }
}
