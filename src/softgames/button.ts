import { Container, Sprite, Text, Texture } from "pixi.js";

export class Button extends Container {
  private back: Sprite;
  private label: Text;
  constructor() {
    super();
    this.back = new Sprite(Texture.WHITE);
    this.back.tint = 0x313131;
    this.back.width = 100;
    this.back.height = 40;
    this.addChild(this.back);

    this.label = new Text("", { fill: 0xffffff, fontSize: "25px" });
    this.label.anchor.set(0.5, 0.5);
    this.addChild(this.label);
    this.text = "";
    this.interactive = true;
  }
  public set text(value: string) {
    this.label.text = value;
    this.back.width = this.label.width + 20;
    this.back.height = this.label.height + 16;
    this.back.position.x = this.label.position.x - this.back.width / 2;
    this.back.position.y = this.label.position.y - this.back.height / 2;
  }
}
