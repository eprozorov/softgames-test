import { Container } from "pixi.js";
import { Button } from "./button";

export class Menu extends Container {
  constructor() {
    super();
    const task1Button = new Button();
    task1Button.text = "Start Task 1";
    this.addChild(task1Button);

    const task2Button = new Button();
    task2Button.text = "Start Task 2";
    this.addChild(task2Button);

    const task3Button = new Button();
    task3Button.text = "Start Task 3";
    this.addChild(task3Button);

    task1Button.position.y = -50;
    task2Button.position.y = task1Button.y + task1Button.height + 10;
    task3Button.position.y = task2Button.y + task2Button.height + 10;

    task1Button.on("pointerdown", () => {
      this.emit("start", 0);
    });
    task2Button.on("pointerdown", () => {
      this.emit("start", 1);
    });
    task3Button.on("pointerdown", () => {
      this.emit("start", 2);
    });
  }
}
