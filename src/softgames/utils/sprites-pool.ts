import { Sprite } from "pixi.js";
import { AbstractPool } from "./abstract-pool";

export class SpritesPool extends AbstractPool<Sprite> {
  protected createNewInstance(): Sprite {
    return new Sprite();
  }
}
