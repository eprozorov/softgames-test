import { Text } from "pixi.js";
import { AbstractPool } from "./abstract-pool";

export class TextsPool extends AbstractPool<Text> {
  protected createNewInstance(): Text {
    return new Text("");
  }
}
