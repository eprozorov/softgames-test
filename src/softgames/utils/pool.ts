export interface IPool<T> {
  obtain(): T;
  release(...objects: T[]): void;
}
