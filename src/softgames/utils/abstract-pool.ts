import { IPool } from "./pool";
export abstract class AbstractPool<T> implements IPool<T> {
  protected readonly objects: T[] = [];

  init(size: number) {
    for (let i = 0; i < size; i++) {
      this.release(this.createNewInstance());
    }
    return this;
  }

  obtain(): T {
    return this.objects.pop() || this.createNewInstance();
  }

  release(...objects: T[]): void {
    this.objects.push(...objects);
  }

  protected abstract createNewInstance(): T;
}
