import { Ticker, TickerCallback } from "pixi.js";

/*
Small helper functions to set/clear intervals using pixi ticker
*/
let intervals: TickerCallback<any>[] = [];
export function setTickerInterval(
  callback: (args: void) => void,
  ms: number
): TickerCallback<any> {
  let progress = 0;
  const seconds = ms / 1000;
  const ticker = (delta: number) => {
    progress += delta;

    const elapsed = progress / (Ticker.shared.FPS * Ticker.shared.speed);

    if (elapsed >= seconds) {
      progress = 0;
      callback();
    }
  };

  Ticker.shared.add(ticker);
  intervals.push(ticker);
  return ticker;
}
export function clearTickerInterval(ticker: TickerCallback<any>) {
  if (ticker) {
    Ticker.shared.remove(ticker);
  }
}
export function clearAllIntervals() {
  for (let t of intervals) {
    Ticker.shared.remove(t);
  }
}
